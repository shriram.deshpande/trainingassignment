import React from "react";
import style from "./Home.module.css";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className={style.container}>
      <Link className={style.navigator} to="/store">
        Store
      </Link>
      <Link className={style.navigator} to="/counter">
        Counter
      </Link>
      <Link className={style.navigator} to="/game">
        Tic Tac Toe
      </Link>
      <Link className={style.navigator} to="/calculator">
        Calculator
      </Link>
      <Link className={style.navigator} to="/timer">
        Timer
      </Link>
    </div>
  );
};

export default Home;

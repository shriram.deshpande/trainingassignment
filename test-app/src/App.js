import Calculator from "./components/Calculator";

import { Route, Routes, useLocation, useParams } from "react-router-dom";
import Card from "./components/Card";

import Counter from "./components/Counter";
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import TicTacToe from "./pages/TicTacToe";
import Timer from "./components/Timer";

function App() {
  const location = useLocation();
  //console.log(location)
  return (
    <div className="App">
      {location.pathname != "/" && <Navbar />}

      <Routes>
        <Route path="/store" element={<Card />} />
        <Route path="/" element={<Home />} />
        <Route path="/counter" element={<Counter />} />
        <Route path="/game" element={<TicTacToe />} />
        <Route path="/calculator" element={<Calculator />} />
        <Route path="/timer" element={<Timer/>}/>
      </Routes>
    </div>
  );
}

export default App;

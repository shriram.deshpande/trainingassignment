import React from "react";
import { Link } from "react-router-dom";
import style from "./Navbar.module.css";

const Navbar = () => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-around",
        padding: "20px 30px",
      }}
    >
      <Link to={"/"} style={{ textDecoration: "none" }} className={style.btn}>
        {" "}
        Home{" "}
      </Link>
      <Link
        to={"/store"}
        style={{
          textDecoration: "none",
        }}
        className={style.btn}
      >
        {" "}
        Store{" "}
      </Link>
      <Link
        to={"/counter"}
        style={{
          textDecoration: "none",
        }}
        className={style.btn}
      >
        {" "}
        Counter{" "}
      </Link>
      <Link
        to={"/game"}
        style={{
          textDecoration: "none",
        }}
        className={style.btn}
      >
        {" "}
        Tic Tac Toe{" "}
      </Link>
      <Link
        to={"/calculator"}
        style={{
          textDecoration: "none",
        }}
        className={style.btn}
      >
        {" "}
        Calculator{" "}
      </Link>
      <Link
        to={"/timer"}
        style={{
          textDecoration: "none",
        }}
        className={style.btn}
      >
        {" "}
        Timer{" "}
      </Link>
    </div>
  );
};

export default Navbar;

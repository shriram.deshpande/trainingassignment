import React from "react";
import { useState } from "react";
import "./Calculator.css";

const Calculator = () => {
  let [result, setResult] = useState("");

  const clickFunc = (e) => {
    setResult(result.concat(e.target.value));
  };

  const clearFunc = () => {
    setResult("");
  };

  const resultFunc = () => {
    setResult(eval(result).toString());
  };

  return (
    <div style={{ marginTop: "5%" }} id="mainbox">
      <h1 style={{ textAlign: "center",fontWeight:"bolder" }}>Calculator</h1>
      <input id="inputbox" type="text" value={result} placeholder="Enter" />
      <div id="allButtons">
        <input
          className="calbutton"
          type="button"
          value="1"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="2"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="3"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="4"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="5"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="6"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="7"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="8"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="9"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="0"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="+"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="-"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="/"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="*"
          onClick={clickFunc}
        />
        <input
          className="calbutton"
          type="button"
          value="%"
          onClick={clickFunc}
        />
        <input
          style={{ backgroundColor: "orange", color: "black" }}
          className="calbutton"
          type="button"
          value="clear"
          onClick={clearFunc}
        />
      </div>
      <input
        className="resultbutton"
        type="button"
        value="="
        onClick={resultFunc}
      />
    </div>
  );
};

export default Calculator;

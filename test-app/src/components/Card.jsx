import React, { useEffect, useState } from "react";
import axios from "axios";
import style from "./card.module.css";

function Card() {
  const [data, setData] = useState("");
  const [renderdData, setRenderData] = useState("");
  const getData = () => {
    axios
      .get(`https://fakestoreapi.com/products`)
      .then((res) => {
        setRenderData(res.data);
        setData(res.data);
      })
      .catch((err) => console.log("404 Not Found"));
  };
  useEffect(() => {
    getData();
    setData(renderdData);
  }, []);
  //   console.log(data);
  const sortMen = () => {
    const mens = renderdData.filter((item) => {
      return item.category === "men's clothing";
    });

    setData(mens);
  };
  const sortWom = () => {
    const mens = renderdData.filter((item) => {
      return item.category === "women's clothing";
    });

    setData(mens);
  };
  const sortEle = () => {
    const mens = renderdData.filter((item) => {
      return item.category === "electronics";
    });

    setData(mens);
  };
  const sortJew = () => {
    const mens = renderdData.filter((item) => {
      return item.category === "jewelery";
    });

    setData(mens);
  };
  const resetFun = () => {
    setData(renderdData);
  };

  return (
    <div>
      <div>
        <p className={style.title}>Store Items </p>
      </div>
      <div className={style.sort_filter_container}>
        <div className={style.filter}>
          <p className={style.categoryTitle}>Categoty Filter</p>

          <div className={style.filter_buttons}>
            <button onClick={sortMen}>Men's Clothing</button>
            <button onClick={sortWom}>Women's Clothing </button>
            <button onClick={sortJew}> Jewelery</button>
            <button onClick={sortEle}>Electronics</button>
            <button onClick={resetFun}>All Products</button>
          </div>
        </div>
      </div>

      <div className={style.container}>
        {/* mapping data to show in UI*/}
        {data ? (
          data.map((item) => {
            return (
              <div key={item.id} className={style.card}>
                <p className={style.category}>{item.category}</p>
                <div className={style.img_div}>
                  <img
                    src={item.image}
                    style={{ width: "200px", height: "240px" }}
                    alt="Product"
                  />
                </div>

                {/* used slice method to hide overflow of lengthy text */}
                <p
                  style={{
                    fontSize: "23px",
                    fontWeight: "600",
                    marginTop: "30px",
                    marginBottom: "30px",
                  }}
                >{`${item.title.slice(0, 20)}...`}</p>
                <p
                  style={{ fontSize: "18px", color: "rgb(92, 90, 90)" }}
                >{`${item.description.slice(0, 90)}...`}</p>
                <div className={style.price_rating_div}>
                  {" "}
                  <p>Price: ₹{item.price}</p>
                  <p>Rating: {item.rating.rate}</p>
                </div>
                <div
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <button className={style.button_cart}>Add To Cart</button>
                  <button className={style.button_wishlist}>
                    Add to Wishlist
                  </button>
                </div>
              </div>
            );
          })
        ) : (
          <div></div>
        )}
      </div>
    </div>
  );
}

export default Card;

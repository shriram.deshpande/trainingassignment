import { background, flexbox, Heading } from '@chakra-ui/react';
import React, { useState } from 'react'
import { useEffect } from 'react';

const Timer = () => {
    const [count , setCount ] = useState(0)

    
       
        useEffect(()=>{
            const id = setInterval(()=>{
               setCount(count=>count+1)
            },1000)
            return () => {
                clearInterval(id)
            }
        },[count])

       
        function resetTimer(){
            setCount(0)
        }
   
  return (
   <>
   <div >
      
        <Heading style={{textAlign: "center" , marginTop:"2rem" ,  background: "#EAF8FF" , color:"#0085FF" , padding:"1rem" , letterSpacing:".1rem" , fontWeight:"800"}}>TIMER </Heading>
        <div style={{display:"flex" , justifyContent:"center" , alignContent:"center" , marginTop:"1rem", fontSize:"18px"}}>

            <span style={{marginRight:"1rem" , fontWeight:"bolder" , fontSize:"23px" , letterSpacing:".05rem"}}>Time : {count}sec</span> 
            <button style={{border:"2px solid #0085FF" , background: "#EAF8FF", cursor:"pointer" , padding:".5rem", width:"8rem" , borderRadius:"4px" , }}onClick={resetTimer}>Reset</button>
        </div>
          
   </div>
    
   </>
  )
}

export default Timer
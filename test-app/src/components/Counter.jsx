import React, { useState } from "react";
import style from "./Counter.module.css";

const Counter = () => {
  const [count, setCount] = useState(0);
  const increment = () => {
    setCount(count + 1);
  };

  const decrement = () => {
    setCount(count - 1);
  };
  const reset = () => {
    setCount(0);
  };

  const styleComp = () => {
    if (count < 0) {
      return "red";
    } else if (count % 2 === 0) {
      return "green";
    } else {
      return "blue";
    }
  };
  return (
    <div id={style.main}>
      <h1
        style={{
          marginTop: "20px",
          fontSize: "40px",
          fontWeight: "bolder",
          textAlign: "center",
        }}
      >
        Counter
      </h1>
      <h1
        style={{
          color: styleComp(),
          fontSize: "30px",
          fontWeight: "bolder",
          textAlign: "center",
          marginTop: "20px",
        }}
      >
        Count: {count}
      </h1>

      <button id={style.Incbutton} onClick={increment}>
        Increment
      </button>
      <button id={style.Resetbutton} onClick={reset}>
        Reset
      </button>
      <button id={style.Decbutton} onClick={decrement}>
        Decrement
      </button>
    </div>
  );
};

export default Counter;
